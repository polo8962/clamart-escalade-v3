<div class="block contenu">

        <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] >= 2){ ?>
        <div class="center"><a href="edition.php?page=<?php echo $page_active; ?>" class="button is-success">Rédaction d'un article</a></div>
        <?php } ?>
        
        <?php
        //On récupère les articles de la page
        $articles_affichage = $bdd->prepare('SELECT * FROM articles WHERE page = ? ORDER BY date_time_publication DESC');
        $articles_affichage->execute(array($page_active));
        ?>
        <?php while($article = $articles_affichage->fetch()){ ?>	
            <div class="article"><!-- On affiche les articles -->
                <br/>
                <?= $article['contenu'] ?>
                
                <div class="datetimepublication">
                    <?= $article['date_time_publication'] ?>
                </div>
                <br/>
                <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] >= 2){ ?>
                    <div class="optionarticle">
                    <a href="edition.php?id=<?php echo $article['id']; ?>" class="button is-link"> Éditer   </a>
                    <a class="button is-danger <?php echo $article['id']; ?>"> Supprimer </a>

                        <div class="modal" id="<?php echo $article['id']; ?>">
                            <div class="modal-background"></div>
                            <div class="modal-card">
                            <header class="modal-card-head">
                            <p class="modal-card-title">Suppression</p>
                            <button class="delete cancel" aria-label="close"></button>
                            </header>
                            <section class="modal-card-body">
                            <p> Voulez vous vraiment supprimer définitivement cet article ? </p>
                            </section>
                            <footer class="modal-card-foot">
                            <a href="suppression.php?id=<?php echo $article['id']; ?>" class="button is-danger">Supprimer</a>
                            <button class="button cancel">Annuler</button>
                            </footer>
                            <script type="text/javascript">
                                //open form connect
                                $('.<?php echo $article['id']; ?>').click(function(){
                                    $('#<?php echo $article['id']; ?>').css(
                                        {
                                            'display' : 'block'
                                        }
                                    );
                                });
                                //close form connect
                                    $('.cancel').click(function(){
                                        $('#<?php echo $article['id']; ?>').css(
                                            {
                                                'display' : 'none'
                                            }
                                        );
                                    });
                                
                            </script>
                        </div>
                        </div>
                    </div>        
                <?php } ?>
            </div>
            <?php } ?>
            <br/>
</div>