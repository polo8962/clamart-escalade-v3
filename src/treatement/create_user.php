<!-- SCRIPT DE VERIFICATION DU FOMRULAIRE -->
<?php 
session_start();
include_once('../treatement/bdd.php');
if($_POST['submit_create_user']){

    //On sécurise les variables
    $identifiant = htmlspecialchars($_POST['create_user_pseudo']);
    $password = sha1($_POST['create_user_password']);
    $type = htmlspecialchars($_POST['create_user_type']);

    //On vérifie si les champs sont remplis
    if(isset($identifiant) AND !empty($identifiant) AND isset($password) AND !empty($password)){

        
        //Si tous les champs sont remplis on envoie les infos a la base de données
        $requser = $bdd->prepare("SELECT * FROM administration WHERE identifiant = ? AND mdp = ?");
        $requser->execute(array($identifiant, $password));
        $userexist = $requser->rowCount();
        
        //On vérifie si le pseudo et le mot de passe correspondent
        if($userexist == 0){

            //Si le pseudo et le mot de passe correspondent alors on déclare l'utilisateur comme étant administrateur et on recharge la page
            $create = $bdd->prepare('INSERT INTO administration (identifiant, mdp, statut) VALUES (?, ?, ?)');
            $create->execute(array($identifiant, $password, $type)); 
            header("Location: ../../administration.php");
        }
        else{
            //Si le pseudo et le mot de passe ne correspondent pas alors on affiche un message d'erreur
            header("Location: ../../administration.php?error_create_user=1");

        }

    }
    else{
        //Si les champs ne sont pas remplis on affiche un message d'erreur
        header("Location: ../../index.php?error_create_user=2");

    }

}

?> 