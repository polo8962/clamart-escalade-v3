<!-- PAGE: CALENDRIER -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "CALENDRIER"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block">

        <div class="calendar">
        <iframe src="https://calendar.google.com/calendar/b/2/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FBrussels&amp;src=Y3NtZS5lc2NhbGFkZUBnbWFpbC5jb20&amp;src=YWIwZ2dhcHVtYXQ4bzNsY3RmcHBxbmdwMzBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=ZnIuZnJlbmNoI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23E67C73&amp;color=%23A79B8E&amp;color=%237986CB&amp;showTz=0&amp;showCalendars=0&amp;showPrint=0&amp;showDate=0&amp;showTitle=0" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
        </div>
            <br/>
        <?php include_once('src/php/affichage_articles.php'); ?>
        </div>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>