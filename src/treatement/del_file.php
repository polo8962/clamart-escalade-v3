<?php 	
session_start();

include_once("bdd.php");

    if(isset($_POST['submit_del_file'])){

        $suppr_id = $_POST['del_file_id'];

        $file_affichage = $bdd->prepare('SELECT * FROM files WHERE id = ? ');
		$file_affichage->execute(array($suppr_id));
		$file = $file_affichage->fetch();

        $suppr = $bdd->prepare('DELETE FROM files WHERE id = ?');
        $suppr->execute(array($suppr_id));
        unlink('../../src/files/'.$file['name']);
        header("Location: ../../administration.php");
    }
?>