﻿<!-- PAGE: ACCUEIL -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->

    <?php 
    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
	$page_active = "ACCUEIL"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

            <!-- SCRIPTS -->
            <?php include_once('src/php/scripts.php'); ?>
            <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <?php include_once('src/php/affichage_articles.php'); ?>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->



    </body>
    <!-- BODY -->

</html>