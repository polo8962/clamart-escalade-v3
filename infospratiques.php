<!-- PAGE: INFOS PRATIQUES -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "INFOS PRATIQUES"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <!-- SCRIPTS -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block contenu">
        <br/>
        <h1 class="title">Choisissez une catégorie :</h1>
        <br/>
        <a href="adhesion.php">
        <section class="hero is-small is-warning is-bold">
        <div class="hero-body center">
            <div class="container">
            <h1 class="title">
            <i class="fas fa-clipboard-list"></i>
                Adhésion :
            </h1>
            <h2 class="subtitle">
                Retrouvez ici toutes les informations sur l'inscription au club 
            </h2>
            </div>
        </div>
        </section>
        </a>

        <a href="horaires.php">
        <section class="hero is-small is-light is-bold">
        <div class="hero-body center">
            <div class="container">
            <h1 class="title">
            <i class="far fa-clock"></i>
                Horaires :
            </h1>
            <h2 class="subtitle">
                Sur cette page vous trouverez les horaires d'ouvertures de la salle du club
            </h2>
            </div>
        </div>
        </section>
        </a>

        <a href="fonctionnement.php">
        <section class="hero is-small is-dark is-bold">
        <div class="hero-body center">
            <div class="container">
            <h1 class="title">
            <i class="fas fa-users-cog"></i>
                Fonctionnement :
            </h1>
            <h2 class="subtitle">
                Ici se trouvent les informations sur le fonctionnement et la gestion du club
            </h2>
            </div>
        </div>
        </section>
        </a>

        <a href="secu_et_materiel.php">
        <section class="hero is-small is-light is-bold">
        <div class="hero-body center">
            <div class="container">
            <h1 class="title">
            <i class="fas fa-hard-hat"></i>
                Sécurité et matériel :
            </h1>
            <h2 class="subtitle">
                Retrouvez le matériel utilisé pour grimper et les informations sur la sécurité 
            </h2>
            </div>
        </div>
        </section>
        </a>

        <a href="encadrement.php">
        <section class="hero is-small is-warning is-bold">
        <div class="hero-body center">
            <div class="container">
            <h1 class="title">
            <i class="far fa-id-card"></i>
                Encadrement :
            </h1>
            <h2 class="subtitle">
                Découvrez nos moniteurs professionels et nos bénévoles
            </h2>
            </div>
        </div>
        </section>
        </a>
        </div>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>

