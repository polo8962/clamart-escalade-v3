    <!-- HEAD -->
    <head>
        <meta charset="utf-8">
        <title>Clamart Escalade: <?php echo($page_active); ?></title>
     
        <!-- PHP -->
        <?php 
            session_start();
            
        ?>
        <!-- PHP -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <!-- CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
        <link rel="stylesheet" href="src/css/main.css">
        <!-- CSS -->

        <!-- FONT -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <!-- FONT -->

    </head>
    <!-- HEAD -->