<!-- BANNER -->
<div class="block">
    <div class="banner">
        <img src="src/img/banner.jpg" alt="banner" class="banner-img"/>
        <div class="banner-content">
            <br/>
            <h1 class="title is-1"><?php echo($page_active); ?></h1>
            <h2 class="subtitle">Bienvenue sur le site de Clamart Escalade</h2>
            <br/>
        </div>  
    </div>
</div>
<!-- BANNER -->