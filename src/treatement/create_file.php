<?php 
session_start();
//On se connecte a la base de donnée pour les articles
include_once("bdd.php");

    if(isset($_SESSION['admin']) AND $_SESSION['admin'] >= 1){
        if(isset($_POST['submit_create_file'])){
            if(isset($_FILES['file_name']) AND !empty($_FILES['file_name']['name'])){

                $name = $_FILES['file_name']['name'];
                
                
                $path = "https://clamartescalade.fr/src/files/".$name;

                $ins = $bdd->prepare('INSERT INTO files (name, path, date) VALUES (?, ?, NOW())');
                $ins->execute(array($name, $path));

                $chemin = "../../src/files/".$name;                
                move_uploaded_file($_FILES['file_name']['tmp_name'], $chemin);
                header("Location: ../../administration.php");  

            }
            else{
                header("Location: ../../administration.php?error_create_file=1");
            }
        }

    }
    else{
        header("Location: ../../index.php");
    }
?>