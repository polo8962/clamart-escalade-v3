<!-- PAGE: PHOTOS -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "ACTUS"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block contenu">
        <?php if(isset($_GET['id'])){ ?>
        <?php
        //On récupère les articles de la page
        $articles_affichage = $bdd->prepare('SELECT * FROM articles WHERE id = ? ');
        $articles_affichage->execute(array($_GET['id']));
        ?>
        <?php while($article = $articles_affichage->fetch()){ ?>	
            <div class="article"><!-- On affiche les articles -->
                <br/>
                <?= $article['contenu'] ?>
                
                <div class="datetimepublication">
                    <?= $article['date_time_publication'] ?>
                </div>
                <br/>
                <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] >= 1){ ?>
                    <div class="optionarticle">
                    <a href="edition.php?id=<?php echo $article['id']; ?>" class="button is-link"> Éditer   </a>
                    <a class="button is-danger del-article"> Supprimer </a>

                        <div class="modal" id="modal_alert">
                            <div class="modal-background"></div>
                            <div class="modal-card">
                            <header class="modal-card-head">
                            <p class="modal-card-title">Suppression</p>
                            <button class="delete cancel" aria-label="close"></button>
                            </header>
                            <section class="modal-card-body">
                            <p> Voulez vous vraiment supprimer définitivement cet article ? </p>
                            </section>
                            <footer class="modal-card-foot">
                            <a href="suppression.php?id=<?php echo $article['id']; ?>" class="button is-danger" >Supprimer</a>
                            <button class="button cancel">Annuler</button>
                            <script>
                            $('.del-article').click(function(){
                                    $('#modal_alert').css(
                                        {
                                            'display' : 'block'
                                        }
                                    );
                                });
                                //close form connect
                                    $('.cancel').click(function(){
                                        $('#modal_alert').css(
                                            {
                                                'display' : 'none'
                                            }
                                        );
                                    });
                                
                            </script>
                            </footer>
                        </div>
                        </div>
                    </div>        
                <?php } ?>
            </div>
            <?php }
        }else{
            echo('  ERREUR: contenu introuvable');
        }
            ?>
            <br/>
        </div>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>