<!-- PAGE: ACTUS -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "ACTUS"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block contenu">
        <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] >= 1){ ?>
        <div class="center"><a href="edition.php?page=<?php echo $page_active; ?>" class="button is-success">Rédaction d'un article</a></div>
        <?php } ?>
        <?php
        $articles_affichage = $bdd->prepare('SELECT * FROM articles WHERE page = ? ORDER BY date_time_publication DESC');
        $articles_affichage->execute(array($page_active));
        ?>
        <?php while($article = $articles_affichage->fetch()){ ?>
            <div class="article"><!-- On affiche les articles -->
                
                <div class="card">
                    <div class="card-image">
                        <div class="center">
                        <img src="src/img/miniature/<?= $article['id'] ?>.jpg" alt='Miniature' width="100%"/>
                        </div>
                    </div>
                    <div class="card-content">

                        <div class="content">
                            <div class="center">
                                <div class="description"><?= $article['descrip'] ?></div><br/>
                                <a class="button is-link" href="articles_actus.php?id=<?= $article['id'] ?>"> Voir l'article </a>
                            </div>
                        <br><br><br>
                            <time datetime="<?= $article['date_time_publication'] ?>"><?= $article['date_time_publication'] ?></time>
                        </div>
                    </div>
                </div>
                </div>

        <?php } ?>
        
        </div>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>