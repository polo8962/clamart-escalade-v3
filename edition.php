<!-- PAGE: EDITION -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "EDITION"; 
    include_once('src/treatement/bdd.php');
    ?>

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

            <!-- SCRIPTS -->
            <?php include_once('src/php/scripts.php'); ?>
            <!-- SCRIPTS -->

    <?php
    session_start();
    $autor = $_SESSION['id'];
    //On modifie un article
    if (isset($_GET['id']) AND !empty($_GET['id'])) { 
        $edit = 1;
        $edit_id = htmlspecialchars($_GET['id']);
        $edit_article = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
        $edit_article->execute(array($edit_id));
        
        if($edit_article->rowCount()== 1){
            $edit_article = $edit_article->fetch();
            $contenu = $edit_article['contenu'];
        }
        else{
            header("Location:".$_SESSION['adresse']);
        }
    }
    //On envoie l'article modifié
    if(isset($_POST['change_article'])){
        if(isset($_POST['article_contenu']) AND !empty($_POST['article_contenu'])){
            $article_contenu = $_POST['article_contenu'];
            $update = $bdd->prepare('UPDATE articles SET contenu = ?, auteur = ?, date_time_publication = NOW() WHERE id = ?');
            $update->execute(array($article_contenu, $autor, $edit_id));
            
            $from = "sitewebclamartescalade@clamartescalade.fr";
            $to = "csme.escalade+websupport@gmail.com";
            $subject = "Modification de l'article n°".$edit_id;
            $message = $article_contenu;
            $headers = "From:" . $from;
            mail($to,$subject,$message, $headers);
            
            header("Location:".$_SESSION['adresse']);
        }
        else{
            $erreur_modif = "Erreur : contenu vide !";
        }
    }
    //On regarde le nouveau contenu
    if(isset($_POST['new_article'])){

        if($_GET['page'] == "ACTUS"){

            if(isset($_POST['article_contenu']) AND isset($_POST['description'])) {
                if(!empty($_POST['article_contenu']) AND !empty($_POST['description'])) { 
                    $article_contenu = $_POST['article_contenu'];
                    $article_page = htmlspecialchars($_GET['page']);
                    $description = $_POST['description'];
      
                        if(isset($_FILES['miniature']) AND !empty($_FILES['miniature']['name'])){

                            if(exif_imagetype($_FILES['miniature']['tmp_name']) == "2"){
                                $ins = $bdd->prepare('INSERT INTO articles (contenu, auteur, date_time_publication, page, descrip) VALUES (?, ?, NOW(), ?, ?)');
                                $ins->execute(array($article_contenu, $autor, $article_page, $description));
                                $lastid = $bdd->lastInsertId(); 
        
                                $chemin = "src/img/miniature/".$lastid.".jpg" ;
                                move_uploaded_file($_FILES['miniature']['tmp_name'], $chemin);

                                $from = "sitewebclamartescalade@clamartescalade.fr";
                                $to = "csme.escalade+websupport@gmail.com";
                                $subject = "Creation de l'actus n°".$lastid;
                                $message = $article_contenu;
                                $headers = "From:" . $from;
                                mail($to,$subject,$message, $headers);


                                header("Location:".$_SESSION['adresse']);     
                            }
                            else{
                                $erreur_modif = "Erreur : Le format de la miniature n'est pas supporté";
                            }
            
            
                        }
                        else{
                            $erreur_modif = "Erreur : Pas de miniature";
                        }
                } 
                else{
                    $erreur_modif = "Erreur : contenu vide !";
                }
      
              }
              else{
                $erreur_modif = "Erreur : contenu vide !";
            }
    }else{

        if(isset($_POST['article_contenu'])) {
          if(!empty($_POST['article_contenu'])) { 
              $article_contenu = $_POST['article_contenu'];
              $article_page = htmlspecialchars($_GET['page']);

                  $ins = $bdd->prepare('INSERT INTO articles (contenu, auteur, date_time_publication, page) VALUES (?, ?, NOW(), ?)');
                  $ins->execute(array($article_contenu, $autor, $article_page));
                  $lastid = $bdd->lastInsertId(); 

                  $from = "sitewebclamartescalade@clamartescalade.fr";
                  $to = "csme.escalade+websupport@gmail.com";
                  $subject = "Creation de l'article n°".$lastid;
                  $message = $article_contenu;
                  $headers = "From:" . $from;
                  mail($to,$subject,$message, $headers);

                  header("Location:".$_SESSION['adresse']);  
          } 
          else{
            $erreur_modif = "Erreur : contenu vide !";
        }

        }
        else{
            $erreur_modif = "Erreur : contenu vide !";
        }
      }
    }
    ?>
    <!-- PHP -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block">
        <?php if(isset($_SESSION['admin']) AND $_SESSION['admin'] >= 1) { ?>
        <?php if(isset($edit)){ ?>
            <h1 class="title">Modification d'un article :</h1>
            <form method="POST">
                <br/>
                <textarea name="article_contenu" placeholder="Contenu de l'article" ><?php echo $contenu; ?></textarea><br /><br/>
                <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'article_contenu' );
                </script>
                <br/>
                <div class="erreur">
                    <?php 
                        if(isset($erreur_modif)){  
                            echo $erreur_modif;
                        }
                    ?>
                </div>
                <div class="center">
                <input type="submit" value="Envoyer l'article" name="change_article" class="button is-link" />
                </div>
            </form>
            <br/>
        <?php } elseif(isset($_GET['page'])) { ?>
            <h1 class="title">Rédaction d'un article :</h1>
            <form method="POST" enctype="multipart/form-data">
            <div class="center">
                <?php if($_GET['page'] == "ACTUS"){ ?>
                <label>Miniature: <input type="file" name="miniature"/></label><br /><br />
                <label>Description: </label><br/><textarea  name="description" maxlength="254"></textarea>
                <?php } ?>
            </div>
                <br/>
                <textarea name="article_contenu" placeholder="Contenu de l'article" ></textarea><br /><br/>
                <script>
                CKEDITOR.replace( 'article_contenu' );
                </script>
                <br/>
                <div class="erreur">
                    <?php 
                        if(isset($erreur_modif)){  
                            echo $erreur_modif;
                        }
                    ?>
                </div>
                <div class="center">
                <input type="submit" value="Envoyer l'article" name="new_article" class="button is-link" />
                </div>
            </form>
            <br/>
        <?php }
    } else{
        header("Location:".$_SESSION['adresse']); 
    }
        ?>
        </div>  
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->


    </body>
    <!-- BODY -->

</html>