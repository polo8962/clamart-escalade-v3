<?php 	
session_start();
//On se connecte a la base de donnée pour les articles
include_once("bdd.php");

    if(isset($_POST['submit_del_user'])){

        $suppr_id = $_POST['del_user_id'];

        $suppr = $bdd->prepare('DELETE FROM administration WHERE id = ?');
        $suppr->execute(array($suppr_id));
        header("Location: ../../administration.php");
    }
?>