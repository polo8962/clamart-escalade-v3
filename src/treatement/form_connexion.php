<!-- SCRIPT DE VERIFICATION DU FOMRULAIRE -->
<?php 
session_start();
include_once('../treatement/bdd.php');
if($_POST['submit']){

    //On sécurise les variables
    $identifiant = htmlspecialchars($_POST['connexion_pseudo']);
    $password = sha1($_POST['connexion_password']);

    //On vérifie si les champs sont remplis
    if(isset($identifiant) AND !empty($identifiant) AND isset($password) AND !empty($password)){

        
        //Si tous les champs sont remplis on envoie les infos a la base de données
        $requser = $bdd->prepare("SELECT * FROM administration WHERE identifiant = ? AND mdp = ?");
        $requser->execute(array($identifiant, $password));
        $userexist = $requser->rowCount();
        $userinfo = $requser->fetch();

        //On vérifie si le pseudo et le mot de passe correspondent
        if($userexist == 1){

            //Si le pseudo et le mot de passe correspondent alors on déclare l'utilisateur comme étant administrateur et on recharge la page
            $_SESSION['admin'] = $userinfo['statut']; 
            $_SESSION['id'] = $userinfo['id'];
            header("Location:".$_SESSION['adresse']);
        }
        else{
            //Si le pseudo et le mot de passe ne correspondent pas alors on affiche un message d'erreur
            header("Location:".$_SESSION['adresse']."?error=1");

        }

    }
    else{
        //Si les champs ne sont pas remplis on affiche un message d'erreur
        header("Location:".$_SESSION['adresse']."?error=1");

    }

}

?> 