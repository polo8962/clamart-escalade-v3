<!-- PAGE: ADMINISTRATION -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "ADMINISTRATION"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <script src="src/js/app_modal_create_user.js"></script>
        <script src="src/js/app_modal_del_user.js"></script>
        <script src="src/js/app_modal_del_file.js"></script>
        <script src="src/js/app_modal_create_file.js"></script>
        <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <div class="block contenu center">

        <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] == 3){ ?>
        <h1 class="title">Gestion des administrateurs</h1>
            <hr/><br/>
        <button class="button is-success" id="create_user_modal">Créer un administrateur</button>
            <br/><br/>
        <button class="button is-danger" id="del_user_modal">Supprimer un administrateur</button>

            <div class="modal" id="create_user">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                    <p class="modal-card-title">Ajouter un Administrateur</p>
                    <button class="delete" aria-label="close" id="close_create_user"></button>
                    </header>
                    <section class="modal-card-body">
                    <form action="./src/treatement/create_user.php" method="POST" >

                        <label>Pseudo :</label>
                        <input class="input" type="text" placeholder="Pseudo" id="create_user_pseudo" name="create_user_pseudo" autocomplete="" required="required" >
                        <label>Mot de passe:</label>
                        <input class="input" type="password" placeholder="Mot de passe" id="create_user_password" name="create_user_password" autocomplete="" required="required" >
                        <br/><br/>
                        <div class="select is-rounded center">
                        <select name="create_user_type" id="create_user_type" required="required">
                                <option value="1">Administrateur restreint</option>
                                <option value="2">Administrateur normal</option>
                                <option value="3">Super administrateur</option>
                        </select>
                        </div>
                        <br />
                        <?php if(isset($_GET['error_create_user'])){ ?>
                        <div class="error">
                            Erreur lors de la création veuillez réessayer !
                        </div>  
                    <?php } ?>
                    </section>
                    <footer class="modal-card-foot">

                        <input type="submit" class="button is-link"  value="Créer" name="submit_create_user">

                    </form>
                    </footer>
                </div>
            </div>
                <?php
                    if(isset($_GET['error_create_user'])){
                ?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#create_user').css(
                                    {
                                        'display' : 'block'
                                    }
                                );
                            });
                        </script>
                <?php
                    }
                ?>



                
            <div class="modal" id="del_user">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                    <p class="modal-card-title">Supprimer un Administrateur</p>
                    <button class="delete" aria-label="close" id="close_del_user"></button>
                    </header>
                    <section class="modal-card-body">
                    <form action="./src/treatement/del_user.php" method="POST">

                    <?php
                        $admin_affichage = $bdd->prepare('SELECT * FROM administration');
                        $admin_affichage->execute();
                    ?>


                        <div class="select is-rounded center">
                        <select name="del_user_id" required="required">
                        <?php 
                            while($admin = $admin_affichage->fetch()){ 
                        ?>
                                <option value="<?php echo($admin['id']); ?>"><?php echo($admin['identifiant']) ?></option>

                            <?php 
                                }
                            ?>
                        </select>
                        </div>
                        <br />

                    </section>
                    <footer class="modal-card-foot">

                        <input type="submit" class="button is-danger"  value="Supprimer" name="submit_del_user">

                    </form>
                    </footer>
                </div>
            </div>

                <?php } ?>

        <!-- ------------------------------------------------------------------------------------------------- -->
            
        <?php if(isset($_SESSION['admin']) and $_SESSION['admin'] >= 1){ ?>
            <br /><br/>     
            <h1 class="title">Gestion des fichiers</h1>
            <hr/><br/>
            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                    <tr>
                        <th colspan="3">Fichier sur le serveur :</th>
                    </tr>
                    <tr>
                            <th>Nom du fichier</th>
                            <th>Chemin du fichier</th>
                            <th>Date d'upload</th>
                </tr>
                </thead>
                <tbody>


            <?php
                $file_affichage = $bdd->prepare('SELECT * FROM files');
                $file_affichage->execute();
            ?>

                        <?php 
                            while($files = $file_affichage->fetch()){ 
                        ?>
                        <tr>
                            <td><?php echo($files['name']); ?></td>
                            <td><?php echo($files['path']); ?></td>
                            <td><?php echo($files['date']); ?></td>
                        </tr>

                            <?php 
                                }
                            ?>
                </tbody>
            </table>

            <br/>
            <button class="button is-success" id="create_file_modal">Ajouter un fichier</button>
            <br/> <br/>
            <button class="button is-danger" id="del_file_modal">Supprimer un fichier</button>

            <div class="modal" id="create_file">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                    <p class="modal-card-title">Ajouter un fichier</p>
                    <button class="delete" aria-label="close" id="close_create_file"></button>
                    </header>
                    <section class="modal-card-body">
                    <form action="./src/treatement/create_file.php" method="POST" enctype="multipart/form-data">

                    <div class="file">
                    <label class="file-label">
                        <input class="file-input" type="file" name="file_name">
                        <span class="file-cta">
                        <span class="file-icon">
                            <i class="fas fa-upload"></i>
                        </span>
                        <span class="file-label">
                            Choisir un fichier
                        </span>
                        </span>
                    </label>
                    </div>
                    <br />
                    <?php if(isset($_GET['error_create_file'])){ ?>
                        <div class="error">
                            Erreur lors de l'importation, veuillez réessayer !
                        </div>  
                    <?php } ?>    
                        

                    </section>
                    <footer class="modal-card-foot">

                        <input type="submit" class="button is-success"  value="Ajouter" name="submit_create_file">

                    </form>
                    </footer>
                </div>
            </div>
            <?php
                    if(isset($_GET['error_create_file'])){
                ?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#create_file').css(
                                    {
                                        'display' : 'block'
                                    }
                                );
                            });
                        </script>
                <?php
                    }
                ?>
            <div class="modal" id="del_file">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                    <p class="modal-card-title">Supprimer un fichier</p>
                    <button class="delete" aria-label="close" id="close_del_file"></button>
                    </header>
                    <section class="modal-card-body">
                    <form action="./src/treatement/del_file.php" method="POST">

                    <?php
                        $fichier_affichage = $bdd->prepare('SELECT * FROM files');
                        $fichier_affichage->execute();
                    ?>


                        <div class="select is-rounded center">
                        <select name="del_file_id" required="required">
                        <?php 
                            while($file = $fichier_affichage->fetch()){ 
                        ?>
                                <option value="<?php echo($file['id']); ?>"><?php echo($file['name']) ?></option>

                            <?php 
                                }
                            ?>
                        </select>
                        </div>
                        <br />

                    </section>
                    <footer class="modal-card-foot">

                        <input type="submit" class="button is-danger"  value="Supprimer" name="submit_del_file">

                    </form>
                    </footer>
                </div>
            </div>

        <?php } ?>


        <!-- ------------------------------------------------------------------------------------------------- -->

        </div>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>