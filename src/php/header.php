<!-- HEADER -->

<div class="block">
    <header class="header">
        <a href="./index.php"><img src="src/img/logo.png" alt="Logo" class="header-logo"/></a>
        <a href="./index.php" class="header-title">
            Clamart Escalade
        </a>
        <nav class="header-menu" id="nav-content">
            <a href="./index.php" class="menu-lien">Accueil</a>
            <a href="./actus.php" class="menu-lien">Actu</a>
            <a href="./infospratiques.php" class="menu-lien">Info. Pratiques</a>
            <a href="./calendrier.php" class="menu-lien">Calendrier</a>
            <a href="./photos.php" class="menu-lien">Photo</a>
            <a href="./contact.php" class="menu-lien">Contact</a>
        </nav>
        <div class="header-icon-menu">
            <a id="icon-menu"><i class="fas fa-bars"></i></a>
        </div>
    </header>
</div>
<?php include_once('modals.php'); ?> 
<?php include_once('redirection.php'); ?> 

<!-- HEADER -->