<!-- PAGE: PHOTOS -->
<!doctype html>
<html lang="fr">

    <!-- PHP -->
    <?php $page_active = "Sécurité et matériels"; 
    include_once('src/treatement/bdd.php');
    ?>
    <!-- PHP -->

    <!-- HEAD -->
    <?php include_once('src/php/head.php'); ?>
    <!-- HEAD -->

        <!-- SCRIPTS -->
        <?php include_once('src/php/scripts.php'); ?>
        <!-- SCRIPTS -->

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <?php include_once('src/php/header.php'); ?>
        <!-- HEADER -->

        <!-- BANNER -->
        <?php include_once('src/php/banner.php'); ?>
        <!-- BANNER -->

        <!-- CONTENT -->
        <?php include_once('src/php/affichage_articles.php') ?>
        <!-- CONTENT -->

        <!-- FOOTER -->
        <?php include_once('src/php/footer.php'); ?>
        <!-- FOOTER -->

    </body>
    <!-- BODY -->

</html>