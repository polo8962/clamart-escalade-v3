<!-- MODALS -->
<div class="modal" id="modal_form">
        <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                <p class="modal-card-title">Connexion</p>
                <button class="delete" aria-label="close" id="close-button"></button>
                </header>
                <section class="modal-card-body">
                <form action="./src/treatement/form_connexion.php" method="POST" id="connection">

                    <label>Pseudo :</label>
                    <input class="input" type="text" placeholder="Pseudo" id="connexion_pseudo" name="connexion_pseudo" autocomplete="username" required="required" >
                    <label>Mot de passe:</label>
                    <input class="input" type="password" placeholder="Mot de passe" id="connexion_password" name="connexion_password" autocomplete="current-password" required="required" >
                    <br />
                    <?php if(isset($_GET['error'])){ ?>
                        <div class="error">
                            Erreur lors de la connexion veuillez réessayer !
                        </div>  
                    <?php } ?>
                </section>
                <footer class="modal-card-foot">

                    <input type="submit" class="button is-link"  value="Se connecter" name="submit">

                </form>
                </footer>
            </div>
    </div>
    <?php
        if(isset($_GET['error'])){
    ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#modal_form').css(
                        {
                            'display' : 'block'
                        }
                    );
                });
            </script>
    <?php
        }
    ?>


<!-- MODAL -->