<?php
session_start();
//On se connecte a la base de donnée pour les articles
include_once("src/treatement/bdd.php");

if(isset($_SESSION['admin']) AND $_SESSION['admin'] >= 2){
	if(isset($_GET['id']) AND !empty($_GET['id']) ){
		$suppr_id = htmlspecialchars($_GET['id']);
		$articles_affichage = $bdd->prepare('SELECT * FROM articles WHERE id = ? ');
		$articles_affichage->execute(array($suppr_id));
		$article = $articles_affichage->fetch();
		
		$from = "sitewebclamartescalade@clamartescalade.fr";
		$to = "csme.escalade+websupport@gmail.com";
		$subject = "Suppresion de l'article n°".$suppr_id;
		$message = $article['contenu'];
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);

		$suppr = $bdd->prepare('DELETE FROM articles WHERE id = ?');
		$suppr->execute(array($suppr_id));
		unlink('src/img/miniature/'.$suppr_id.'.jpg');
		
			
 

		header("Location: index.php");
		
	}
}
else{
	header("Location: index.php");
}
//Code pour supprimer un article
?>